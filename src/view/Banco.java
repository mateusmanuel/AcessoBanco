package view;

import model.Conta;
import model.Deposito;
import model.Saque;

public class Banco {

	public static void main(String[] args) {
		
		Conta umaConta = new Conta();
		Saque umSaque = new Saque(umaConta);
		Deposito umDeposito = new Deposito(umaConta);
		
		System.out.println("Saldo Inicial = " + umaConta.getSaldo());
		
		umSaque.start();
		umDeposito.start();
		
		while(umDeposito.getState()!=Thread.State.TERMINATED || umSaque.getState()!=Thread.State.TERMINATED)
		{}
		
		System.out.println("Saldo Final = " + umaConta.getSaldo());
	}

}
