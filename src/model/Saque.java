package model;

public class Saque extends Thread {

	private Conta umaConta;
	
	public Saque (Conta umaConta){
		this.umaConta = umaConta;
	}
	
	@Override
	public void run() {
		
		for (int i = 0; i < 100; i++) {

			try{	
				umaConta.sacar(100);
			}
			catch (IllegalArgumentException e) {
				System.out.println("Impossível Sacar. Saldo Insuficiente!");
			}
			System.out.println("Saldo Parcial (Saque) = " + umaConta.getSaldo());
		}
		
		System.out.println("Término dos Saques");
	}
}
