package model;

public class Conta {

	private double saldo = 0;
	private double temp;
	
	public double getSaldo(){
		return saldo;
	}
	
	public void setSaldo(double saldo){
		this.saldo = saldo;
	}
	
	public synchronized void sacar (double valor) {
		temp = getSaldo();
		
		if (temp < valor){
			throw new IllegalArgumentException("Não há Saldo para Saque|");
		}
		else{
			saldo = temp - valor;
		}
	}
	
	public synchronized void depositar (double valor){
		temp = getSaldo();
		saldo = temp + valor;
	}
}
