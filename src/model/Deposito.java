package model;

public class Deposito extends Thread{
	
	private Conta umaConta;
	
	public Deposito (Conta umaConta){
		this.umaConta = umaConta;
	}
	
	@Override
	public void run() {
		
		for (int i = 0; i < 100; i++) {
			
			umaConta.depositar(100);
			System.out.println("Saldo Parcial (Depósito) = " + umaConta.getSaldo());
		}		
		System.out.println("Término dos Depósitos");
	}

}
